var searchData=
[
  ['i2c_5f10_5fbit_5faddr',['I2C_10_BIT_ADDR',['../mdev__i2c_8h.html#a0745964a9927ad536e6b3c46781d86d8',1,'mdev_i2c.h']]],
  ['i2c_5fclk_5f100khz',['I2C_CLK_100KHZ',['../mdev__i2c_8h.html#a0090783ddbea3de8c9ef3dccd07e6c58',1,'mdev_i2c.h']]],
  ['i2c_5fclk_5f10khz',['I2C_CLK_10KHZ',['../mdev__i2c_8h.html#aa4f892488b3eef20f2c39c55ac221ffb',1,'mdev_i2c.h']]],
  ['i2c_5fclk_5f400khz',['I2C_CLK_400KHZ',['../mdev__i2c_8h.html#abad4bd544233a58a5134fd48bbe33bcd',1,'mdev_i2c.h']]],
  ['i2c_5fdevice_5fslave',['I2C_DEVICE_SLAVE',['../mdev__i2c_8h.html#a9b126088561b1cb0e9883aa89cf596f9',1,'mdev_i2c.h']]],
  ['i2c_5fslaveadr',['I2C_SLAVEADR',['../mdev__i2c_8h.html#a34a991c799d3da0e90db2c5d387cf14d',1,'mdev_i2c.h']]]
];
