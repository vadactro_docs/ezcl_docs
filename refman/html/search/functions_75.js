var searchData=
[
  ['uart_5fdrv_5fblocking_5fread',['uart_drv_blocking_read',['../mdev__uart_8h.html#a17d0eab785e22b91e1250d6d12b83c9c',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fclose',['uart_drv_close',['../mdev__uart_8h.html#a9cc8b7e52a2c9fa6041fb6693bbd8043',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fdeinit',['uart_drv_deinit',['../mdev__uart_8h.html#a7667a2fac09f3f7ef90be946761e236f',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fdma_5frd_5fblk_5fsize',['uart_drv_dma_rd_blk_size',['../mdev__uart_8h.html#a7c6dcc36d6f470545bf66d503b9b1c88',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fget_5fportid',['uart_drv_get_portid',['../mdev__uart_8h.html#a6cf226287eb45e32281f7caeb16a4ed9',1,'mdev_uart.h']]],
  ['uart_5fdrv_5finit',['uart_drv_init',['../mdev__uart_8h.html#a088982f0efe9be9cf27f6db78b4c8fe8',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fopen',['uart_drv_open',['../mdev__uart_8h.html#afb2488812d6b2788b2f97edd718511f2',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fread',['uart_drv_read',['../mdev__uart_8h.html#a67a94b4ff910dbd0af225f5072df1d5c',1,'mdev_uart.h']]],
  ['uart_5fdrv_5frx_5fbuf_5freset',['uart_drv_rx_buf_reset',['../mdev__uart_8h.html#a200f46c3cad831b2a9dc8452ad422cb6',1,'mdev_uart.h']]],
  ['uart_5fdrv_5frxbuf_5fsize',['uart_drv_rxbuf_size',['../mdev__uart_8h.html#a19fb0886d66cb339d33e5d73426a2e3f',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fset_5fopts',['uart_drv_set_opts',['../mdev__uart_8h.html#a4e307dd033ff09196e3fcc29900ff90a',1,'mdev_uart.h']]],
  ['uart_5fdrv_5ftimeout',['uart_drv_timeout',['../mdev__uart_8h.html#a471254e90c8e0d7f136f1218199a683e',1,'mdev_uart.h']]],
  ['uart_5fdrv_5ftx_5fflush',['uart_drv_tx_flush',['../mdev__uart_8h.html#adbc7580c0b1d813332a36090c43a3c5e',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fwrite',['uart_drv_write',['../mdev__uart_8h.html#af6056a5ae7f8099f75ebf3289d670509',1,'mdev_uart.h']]],
  ['uart_5fdrv_5fxfer_5fmode',['uart_drv_xfer_mode',['../mdev__uart_8h.html#a4d10f943f078baf3443a0e2324137512',1,'mdev_uart.h']]]
];
