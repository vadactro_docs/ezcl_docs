[Home](../../#quick-start-guide-1-get-the-development-boards)

# VADACTRO's IoT Evaluation Kit
<a href="mailto:sales@edu.vadactro.org.in" target="_blank" class="button">Buy Now</a>

### Introduction

[Marvell MW302 Based IoT Evaluation Kit by VADACTRO](http://edu.vadactro.org.in)
The kit is powered by Marvell EZ-Connect MW300/302 Wi-Fi microcontroller system-on-chip (SoC), a single-chip SoC with 1x1 802.11n Wi-Fi and full-featured Cortex-M4 microcontroller.  The SoC includes 512kB SRAM and a flash controller to enable executing code from external QSPI Flash. The SoC also enables easy interfacing to sensors, actuators, and other components via a full set of I/O interfaces including SPI, I2C, UART, I2S, PWM, ADC, DAC etc.  The kit includes a set of IO headers that bring out these interfaces to connect to external sensor or other peripheral boards.

* 32-bit Cortex M4F at 200Mhz
* 512KB SRAM
* 4MB flash with XIP support
* 802.11 b/g/n with FCC, IC, CE certification
* 3 user LEDs & 3 user button
* Micro-USB Connector for DC Power Input
* Wide Power Supply Input voltage range (3.3V to 24V DC)
* Jumper Provision for RTC Calibration and Boot option Selection
* Arduino Compatible Connectors Interface
* Extra 4-Pin Connectors for I2C and UART interfaces
* SWD, JTAG, Debug-UART Interfaces for debugging

### WiFi MCU board

<a href = "./VD-IoT-EvalKit-Features.png"> <img src="./VD-IoT-EvalKit-Features.png" width=100%> </a>


VD-IOT-EVALKIT-WIFIB is the main board having MW300 WiFi MCU Module at its heart. It exposes all the possible connections/pins  that are needed for application as well as debugging interface. It also has three on-board LEDs (Red, Green and Blue)  and Three Push buttons (Reset, Reset-to-Provisioning and UserPB). These make it easy to test sample applications.

VD-IOT-EVALKIT-WIFIB doesn't provide an onboard JTAG debugger interface to keep the dev board form factor and cost low. It comes with a 10Pin header interface which can be used to connect debug interface externally

VD-IOT-EVALKIT has a small form factor and it's Arduino compatible header interface and wide operating input voltage range makes it convenient to construct any IoT gadget quickly as shown below

<img src="https://s15.postimg.org/8plhxwq17/Evalkit_with_base_Shield_Debug_B_Sensor_Connected.jpg" width="100%"></img>

### FTDI Based Debugger Board

To make the VD-IOT-EVALKIT-WIFI board work with Eclipse IDE using OpenOCD, and get complete hardware debugging, it is necessary to use an external debugger.

<img src="./VD-IOT-EVALKIT-DEBUG-WIFI-Small.jpg" width="100%"></img>

This Debug board (VD-IOT_EVALKIT-DEBUGGERT) is optional but bundled with VD-IOT-EVALKIT. It based on FTDI chipset, and offers very simple and complete debug interface to do any level of debugging. It is plug-on adapter board specially developed for debugging purpose and is compatible with default debug configuration of EZ-Connect-Lite SDK.

It provides JTAG as well as Debug UART interface. As well as it supplies power to the main WiFi board that is more than enough for any kind of debugging needs.

<img src="./VD-IOT-EVALKIT-DEBUG-WIFI-PC-Small.jpg" width="100%"></img>

### Board Schematics

Download board schematics from here [SCH_VD-IOT-EVALKIT-WIFIB-V12P.PDF](https://gitlab.com/VD-EDU/vd-iot-evalkit-sdk/uploads/880725b5ee22ceab4038121aa0d3a7a3/SCH_VD-IOT-EVALKIT-WIFIB-V12P.PDF)

### Jumper Settings
You can locate the jumper location on the board using below diagram

<a href = "./VD-IoT-EvalKit-Jumpers_locator.png"> <img src="./VD-IoT-EvalKit-Jumpers_locator.png" width=75%> </a>

Jumpers and their usage description

| Jumper  | Usage |
| :------------ | :-----:|
| J6 (Clock Calibration)     | Insert this jumper if RTC clock to be calibrated from crystal |
| J7 (Boot Option on GPIO_16)    | Configures boot option, for normal operation do not polulate |
| J5 (Boot Option on GPIO_27)    | Configures boot option, for normal operation do not polulate |

For more details please refer product [specifications](http://www.marvell.com/microcontrollers/88MW300/302/)

### Borad Swtiches

There are four configurable swiches on the board, of which two are already configured in the SDK and can be used by end application for any need.
 
| SWITCH  | GPIO Used | Usage |
| :------------ |:---------------:| :-----:|
| SW3 (User PushBtn)     | GPIO_22 | board_button_1() |
| SW2 (NEtwork PushBtn)    | GPIO_39 | board_button_2() |
| SW1     | RESET | System Reset |

### Board LEDs

You can use below LEDs in your application for indication purpose, those are configured in the SDK as explained below

| LED  | GPIO Used | Usage |
| :------------ |:---------------:| :-----:|
| LED1(RED)     | GPIO_1 | board_led_1() |
| LED2(BLUE)     | GPIO_0 | board_led_2() |
| LED3(GREEN)     | GPIO_4 | board_led_3() |

### Pin Map

**NOTE** : The VD-IoT-Evalkit Board is 3.3v tolerant. If you are using a 5v sensor or module, please make sure that you shift down the voltage levels using a resistances or a level-shifter IC. **5v input can damage the module**.

<a href = "https://gitlab.com/VD-EDU/vd-iot-evalkit-sdk/uploads/3bd07c1501cd7da1f650dbefc6e38d5c/VD-IOTM-EVALKIT-V1P-PinMap-small.png"> <img src="https://gitlab.com/VD-EDU/vd-iot-evalkit-sdk/uploads/3bd07c1501cd7da1f650dbefc6e38d5c/VD-IOTM-EVALKIT-V1P-PinMap-small.png" width=100%> </a>

(click image to open larger version)

### Arduino Compatible Header Interface

- VD-IOT-EVALKIT has 4 Arduino Compatble Headers, which are friendly enough to connect any standard and non standard companion interface like Grove Base Shield.It also has two 4 pin headers for I2S and UART interfaces and a 10Pin debug interface for easy SWD/JTAG programming.
- The header file where all the pin functions are defined is [mw300_pinmux.h](https://github.com/marvell-iot/ez-connect-lite/blob/master/sdk/src/incl/sdk/drivers/mw300/mw300_pinmux.h)

<a href = "./VD-IoT-EvalKit-Headers.png"> <img src="./VD-IoT-EvalKit-Headers.png" width=100%> </a>

<strong>Header-1 : IOL - GPIOs </strong>

| Pin No_#  | Function 0  | Function 1 | Function 2 |
| :------------ |:---------------:| :-----:| :-----:|
| 1      | GPIO_03 | UART0_RXD | - |
| 2      | GPIO_02 | UART0_TXD | - |
| 3      | GPIO_00 | - | - |
| 4      | GPIO_01 | - | - |
| 5      | GPIO_04 | - | - |
| 6      | GPIO_05 | - | - |
| 7      | GPIO_24 | - | - |
| 8      | GPIO_23 | WAKE_UP1 | - |

<strong>Header-2 : Power Interface </strong>

| Pin_No_#  | Function  | Description |
| :------------ |:---------------:| :-----:|
| 1      | VIN | 3.3V to 24V DC Power Input |
| 2      | GND | Power Ground |
| 3      | GND | Power Ground |
| 4      | +5V | 5V Power intput or ourput in case powered using micro-USB |
| 5      | +3.3V | 3.3V Output of Internal regulartor |
| 6      | RESET | System Reset Input |
| 7      | D- | Brought out Micor-USB data signal on connector for application use, if nay |
| 8      | D+ | Brought out Micor-USB data signal on connector for application use, if nay |

<strong>Header-3 : IOH - GPIOs </strong>

| Pin_No_#  | Function 0  | Function 1 | Function 2 |
| :------------ |:---------------:| :-----:| :-----:|
| 1      | GPIO_22 | WAKE_UP0 | - |
| 2      | GPIO_15 | ACLOCK | - |
| 3      | GPIO_47 | SSP2_FRM | - |
| 4      | GPIO_48 | SSP2_TXD | - |
| 5      | GPIO_49 | SSP2_RXD | - |
| 6      | GPIO_46 | SSP2_CLK | - |
| 7      | GND | - | - |
| 8      | N/C | AREF | - |
| 9      | GPIO_25 | SDA | - |
| 10     | GPIO_26 | SCL | - |


<strong>Header-4 : Analog - GPIOs </strong>

| Pin_No_#  | Function 0  | Function 1 | Function 2 |
| :------------ |:---------------:| :-----:| :-----:|
| 1      | GPIO_42 | ADC_CH2 | - |
| 2      | GPIO_43 | ADC_CH3 | - |
| 3      | GPIO_44 | ADC_CH4 | - |
| 4      | GPIO_45 | ADC_CH5 | - |
| 5      | GPIO_40 | ADC_CH0 | - |
| 6      | GPIO_41 | ADC_CH1 | - |

### SDK Build customization

This is not default configured board in the EZ Connect Lite SDK, and the associated board file is `vd_iot_evalkit.c`. If you do not explicitly mention a  `BOARD_FILE` while building the SDK or a single `APP, it will build for other board.
So please make sure you configure additional build parameter to build for this board 

```
i.e. BOARD=vd_iot_evalkit
```
Here's how to compile a simple `hello_world` for AWS Starter Kit -

```
make APP=sample_apps/hello_world BOARD=vd_iot_evalkit
```

### Board binaries
The generated binaries can be found in the `bin/vd_iot_evalkit/`


####[Back to Main Menu](../../#quick-start-guide-1-get-the-development-boards)

