[Home](../../#quick-start-guide-3-setup-eclipse)

# Setup Eclipse

This link documents how to setup Eclipse as a development IDE for the available development boards to work with Ez-Connect-Lite.


### Install prerequisites

Depending on your platform, assumption here is that you have prepresed your development host, if not please follow the instrauction [here](../../../#quick-start-guide-2-setup-development-host)

### Install Eclipse IDE for C/C++

* EZ-Connect-Lite SDK supports the latest Eclipse version (at present Oxygen). This version is actively used for SDK validation. We strongly recommend to use this version for your development. Whereas EZ-Connect Lite SDK also supports Mars as well as Neon versions. **Note** that Eclipse Neon has known issues with debug launchers. We have instructions for a workaround in respective sections.
 - Eclipse Oxygen [Download Installer](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R2/eclipse-inst-linux32.tar.gz)
 - Eclipse Neon [Download](http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/neonr)
 - Eclipse Mars [Download](http://www.eclipse.org/downloads/packages/eclipse-ide-cc-developers/mars2)

* Extract it and launch the executable. If you are launching installer then install **Eclipse IDE for C/C++ Developers**. Further during installation it will ask to trust the certificates, selecte all and accept to continue.

* If this is first time that you are launching eclipse, it will ask for workspace selection, go with default setup and continue.

* In new welcome window you will be asked to create project - just close this and follow below instruction to install more addon plugins needed

<img src="./eclipse_welcome_window.png" width=100%>

### Install addon plugins

You need to install C/C++ GDB Hardware Debugging plugin that help for flash programming and debugging

Install the this plugins oepn Eclipse IDE, from Help -> Install New Software
You will get install window pop-up, go to 'work with" drop down list and select "--All Available Sites--" as shown below.

<img src="./eclipse_plugin_installation_step1.png" width=75%>

As a result of above operation, it will show up all packages, now go to filter dialog box any enter filter text as "C/C++ GDB Hardware Debugging", you will see plugin pointers as shown below. Select the needed onw and hit the "Next" button and complete the installation process

<img src="./eclipse_plugin_installation_step2.png" width=75%>

After plugin installation, please restart explipse again vefore using it further.

### Get EZ-Connect-Lite SDK

#### Import Eclipse git project (EZ-Connect-Lite SDK)

We will now import the project into Eclipse using git.

* File -> Import

<img src="./import.png" width=100%>

* Git -> Projects from Git -> Clone URI

<img src="./eclipse_getsdk_step1.png" width=50%>

* In this setup you will be asked to enter git project url, you can give below url to EZ-Connect-Lite SDK hosted on GitHub

`https://gitlab.com/marvell-iot/ez-connect-lite `

<img src="./eclipse_getsdk_step2.png" width=50%>

* Press Next it will prompt for **Local Destination** configuration, by default Eclipse IDE will select some default destination folder, but you can change it to your needed location as shown below

<img src="./eclipse_getsdk_step3.png" width=50%>

* Press Next and then Finish the import process with all default settings.

The project along with the settings has been now imported in your Eclipse IDE.

### Avoiding root previleges

**NOTE:** This step only to be followed if your development host is **Linux.**

To use OpenOCD or Serial Terminal on console in Eclipse IDE, user needs root previleges. Both these tools are needed for interacting with development board. Below script **perm_fix.sh** provided in SDK need to be executed from terminal to enable Eclipse to access these tools without root previllidges

`./sdk/tools/bin/perm_fix.sh`

<img src="./eclipse_getsdk_step4.png" width=75%>

* After execution of this stript, you **MUST** logout and re-login to the host

### Setup ARM Cross-compiler Toolchain Path
On Windows plartform you may skip this step since the path is automatically set by the installation process. But on Linux plarform you need to configure toolchain patch in the Ecliplse IDE to build EZ-Connect-Lite SDK project

* Go to Project → Properties → C/C++ Build → Environment.
* Click on Add button, enter in the Name field PATH, since this is system variable, you will see predefined values to it, edit and add the ARM Cross-compiler toolchain installation path on your system to it.
* Click on Apply to save it.
* Finally click on Ok to apply all changes.

<img src="./eclipse_setpath_for_build.png" width=100%>

### Setup BOARD Environment Variable
**OPTIONAL Setup :** EZ-Connect-Lite SDK supports multiple development boards from different sources, you may procure any of these for your development. SDK have built in support for all these boards. By default SDK is build for MW300_RD board (i.e. AWS IoT Starter Kit). You can always build the SDK for your desired board by passing additional build environment variable.

For ex: If you planned to use [VD-IoT-EvalKit](../vd-iot-evalkit) instead of defult AWS IoT Starter Kit then create additional build environment variable name as **BOARD** with value **vd_iot_evalkit**.

* Go to Project → Properties → C/C++ Build → Environment.
* Click on Add button, enter in the Name field BOARD and in the Value field as vd_iot_evalkit.
* Click on Apply to save it.
* Finally click on Ok to apply all changes.


### Setup XIP Environment Variable

#### What is XIP mode?
The MW3XX supports eXecute-In-Place (XIP). With this feature, the microcontroller can directly execute code from flash, without having to load it entirely into the RAM first. With this feature enabled, applications get much larger room for:

* code (text section): since it can be stored in as large a flash as possible
* as well as data (bss, data sections): larger free RAM is available, since the code isn’t getting loaded into the RAM

By default, the EZ-Connect-Lite builds images with XIP disabled. You can enable XIP by simply following below setup procedure.

**OPTIONAL Setup :** By default few applications (like aws_demo) do not build since this need XIP featureneed to be enabled in the SDK. This settings is optional and can only be configured if needed. You need extra environment variable to be set with name as **XIP** and value as **1**.

* Go to Project → Properties → C/C++ Build → Environment.
* Click on Add button, enter in the Name field XIP and in the Value field as 1.
* Click on Apply to save it.
* Finally click on Ok to apply all changes.


####[Go to Quick Start Guide: 4. Build Sample Applications](../../#quick-start-guide-4-build-sample-applications)

####[Back to Main Menu](../../#quick-start-guide-3-setup-eclipse)
